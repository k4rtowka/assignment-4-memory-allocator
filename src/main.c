#include "mem.h"
#include <assert.h>
#include <malloc.h>

void success_alloc();
void free_one_block();
void free_two_blocks();
void expand_old_region();
void expand_old_region_v2();

int main() {
    printf("Tests started.\n");

    success_alloc();
    free_one_block();
    free_two_blocks();
    expand_old_region();
    expand_old_region_v2();
}

void success_alloc() {
    void *heap = heap_init(8175);

    assert(heap);
    debug_heap(stdout, heap);

    void *block = _malloc(32);
    assert(block);
    debug_heap(stdout, heap);

    _free(block);
    debug_heap(stdout, heap);
    heap_term();
    printf("---------Test 1: succeed---------\n");
}

void free_one_block() {
    void *heap = heap_init(8175);
    debug_heap(stdout, heap);

    void *block1 = _malloc(32);
    void *block2 = _malloc(32);
    void *block3 = _malloc(32);
    void *block4 = _malloc(32);

    debug_heap(stdout, heap);
    
    assert(block1);
    assert(block2);
    assert(block3);
    assert(block4);

    _free(block2);
    debug_heap(stdout, heap);

    _free(block1);
    _free(block3);
    _free(block4);
    heap_term();
    printf("---------Test 2: succeed---------\n");
}

void free_two_blocks() {
    void *heap = heap_init(8175);
    debug_heap(stdout, heap);

    void *block1 = _malloc(32);
    void *block2 = _malloc(32);
    void *block3 = _malloc(32);
    void *block4 = _malloc(32);

    debug_heap(stdout, heap);
    
    assert(block1);
    assert(block2);
    assert(block3);
    assert(block4);

    _free(block3);
    _free(block2);

    debug_heap(stdout, heap);

    _free(block1);
    _free(block4);
    heap_term();
    printf("---------Test 3: succeed---------\n");
}

void expand_old_region() {
    void *heap = heap_init(8175);
    debug_heap(stdout, heap);

    void *block1 = _malloc(8176);
    debug_heap(stdout, heap);
    assert(block1);
    _free(block1);
    heap_term();
    printf("---------Test 4: succeed---------\n");
}

void expand_old_region_v2() {
    const int h_size = 1024 * 8;
    const int h_cap = h_size - 17;
    const int block_size = 1024;

    void *heap = heap_init(h_cap);
    debug_heap(stdout, heap);

    void* block_1 = _malloc(h_cap);
    assert(block_1);
    (void) mmap((void*)HEAP_START + h_size, block_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    debug_heap(stdout, heap);

    void* block_2 = _malloc(block_size);
    assert(block_2);
    debug_heap(stdout, heap);

    _free(block_1);
    _free(block_2);

    heap_term();
    printf("---------Test 5: succeed---------\n");
}
